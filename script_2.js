function SteeringWheel(diameter){
   var _diameter = diameter;
    this.setDiameter = function(diameter) {
      _diameter = diameter;
    }
    this.getDiameter = function() {
      return _diameter;
    }
};


function Wheel(diameter, preasure){
   var _diameter = diameter;
   var _preasure = preasure;

      this.setDiameter = function(diameter) {
      _diameter = diameter;
    }
    this.getDiameter = function() {
      return _diameter;
    }

    this.setPreasure = function(preasure) {
      _preasure = preasure;
    }
    this.getPreasure = function() {
      return _preasure;
    }
}

function CarBody(color, width, height) {
    var _color = color;
    var _width = width;
    var _height = height;

    this.setColor = function(color) {
      _color = color;
    }
    this.getColor = function() {
      return _color;
    }

    this.setWidth = function(width) {
      _width = width;
    }
    this.getWidth = function() {
      return _width;
    }

    this.setHeight = function(height) {
      _height = height;
    }
    this.getHeight = function() {
      return _height;
    }
}

function Machine(model, engine) {
 this.model = model;
 this.engine = engine;

 this.steering_wheel = new SteeringWheel();
 this.wheel = new Wheel();
 this.car_body = new CarBody();
}

Machine.prototype.setCarBodyColor = function(color) {
  this.car_body.color = color;
}
Machine.prototype.getCarBodyColor = function() {
  return this.car_body.color;
}

Machine.prototype.setCarBodyWidth = function(width) {
  this.car_body.width = width;
}
Machine.prototype.getCarBodyWidth = function() {
  return this.car_body.width;
}

Machine.prototype.setCarBodyHeight = function(height) {
  this.car_body.height = height;
}
Machine.prototype.getCarBodyHeight = function() {
  return this.car_body.height;
}

Machine.prototype.setWheelPreasure = function(preasure) {
  this.wheel.preasure = preasure;
}
Machine.prototype.getWheelPreasure = function() {
  return this.wheel.preasure;
}

Machine.prototype.setWheelDiameter = function(diameter) {
  this.wheel.diameter = diameter;
}
Machine.prototype.getWheelDiameter = function() {
  return this.wheel.diameter;
}

Machine.prototype.setSteeringWheelDiameter = function(diameter) {
  this.steering_wheel.diameter = diameter;
}
Machine.prototype.getSteeringWheelDiameter = function() {
  return this.steering_wheel.diameter;
}

var machine_1 = new Machine();
machine_1.model = 'super car';
machine_1.engine = '1.5 TDI'
machine_1.setCarBodyColor('yellow');
machine_1.setCarBodyWidth(120);
machine_1.setCarBodyHeight(300);
machine_1.setWheelPreasure(2.2);
machine_1.setWheelDiameter(17);
machine_1.setSteeringWheelDiameter(45);

console.log(machine_1.model);
console.log(machine_1.engine);
console.log(machine_1.getCarBodyColor());
console.log(machine_1.getCarBodyWidth());
console.log(machine_1.getCarBodyHeight());
console.log(machine_1.getWheelPreasure());
console.log(machine_1.getWheelDiameter());
console.log(machine_1.getSteeringWheelDiameter());
