function HourlyEmployee(worked_time, per_hour_rate){
   this.worked_time = worked_time;
   this.per_hour_rate = per_hour_rate;
}

function FixedRateEmployee(month_rate){
   this.month_rate = month_rate;
}

HourlyEmployee.prototype.getSalary = function() {
  return 'year salary is : ' + this.worked_time * this.per_hour_rate * 12;
};
FixedRateEmployee.prototype.getSalary = function() {
  return 'year salary is : ' + this.month_rate * 12;
};

var person_1 = new HourlyEmployee(180, 15);
var person_2 = new FixedRateEmployee(1250);

console.log('person_1 ' + person_1.getSalary());
console.log('person_2 ' + person_2.getSalary());
